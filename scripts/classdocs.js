function apidocs_init(){
    $.getJSON("EventDispatcher.json",function(res){
        console.log(res);
        let node=$('#methods');
        node.html("");
        node.append('<h2 class="off-left">Methods</h2>');
        let methods=res.methods;
        for(let i=0;i<methods.length;i++){
            node.append(createMethodNode(methods[i]));
        }
        prettyPrint();
    });
    let link='https://developer.mozilla.org/en/JavaScript/Reference/Global_Objects/';
    /**
     * 创建method根节点
     * @param {Array} data 
     * @returns 
     */
    function createMethodNode(data){
        let node=$('<div id="method_'+data.name+'" class="method item"></div>');
        node.append('<h3 class="name"><code>'+data.name+'</code></h3>');
        node.append(createArgsNode(data.parameters));
        if(data.returns){
            node.append(createReturnsLineSpan(data.returns.type));
        }
        node.append(createMetaNode(data.meta));
        node.append(createDescriptionNode(data.des));
        if(data.parameters){
            node.append(createParamsNode(data.parameters));
        }
        if(data.returns){
            node.append(createReturnsNode(data.returns));
        }
        return node;
    }
    /**
     * 参数根节点
     * @param {Array} data 
     * @returns 
     */
    function createArgsNode(data){
        let node=$('<div class="args"></div>');
        node.append('<span class="paren">(</span>');
        data&&node.append(createArgsListNode(data));
        node.append('<span class="paren">)</span>');
        return node;
    }
    /**
     * 参数列表
     * @param {Array} data 
     * @returns 
     */
    function createArgsListNode(data){
        let node=$('<ul class="args-list inline commas"></ul>');
        for(let i=0;i<data.length;i++){
            node.append(createArgNode(data[i]));
        }
        return node;
    }
    /**
     * 参数
     * @param {Object} data 
     * @returns 
     */
    function createArgNode(data){
        let node=$('<li class="arg"></li>');
        let html='<code> '+data.name+' </code>';
        if(data.flag=='optional'){
            html='<code class="optional">['+data.name+']</code>';
        }
        node.append(html);
        return node;
    }
    /**
     * 返回类型
     * @param {Array|String} data 
     * @returns 
     */
    function createReturnsLineSpan(data){
        let pnode=$('<span class="returns-inline"></span>');
        /*let node=$('<span class="type"></span>');
        if(typeof data=='string'){
            node.append(createCrossLink(data));
        }else{
            for(let i=0;i<data.length;i++){
                node.append(createCrossLink(data[i]));
                if(i<data.length-1)
                node.append(' | ')
            }
        }*/
        pnode.append(createTypeNode(data));
        return pnode;
    }
    /**
     * 类型对应的链接
     * @param {String} name 
     * @returns 
     */
    function createCrossLink(name){
        let node=$('<a href="'+link+name+'" class="crosslink external" target="_blank">'+name+'</a>');
        return node;
    }
    /**
     * 定义
     * @param {Object} data 
     * @returns 
     */
    function createMetaNode(data){
        let node=$('<div class="meta"></div>');
        let p=$('<p> Defined in </p>');
        let a=$('<a href="../files/"'+data.line+'><code>'+data.des+'</code></a>');
        p.append(a);
        node.append(p);
        return node;
    }
    /**
     * 描述
     * @param {String} des 
     * @returns 
     */
    function createDescriptionNode(des){
        let node=$('<div class="description"></div>');
        node.append(des);
        return node;
    }
    /**
     * Params
     * @param {Array} data 
     * @returns 
     */
    function createParamsNode(data){
        let node=$('<div class="params"></div>');
        node.append('<h4>Parameters:</h4>');
        let list=$('<ul class="params-list"></ul>');
        for(let i=0;i<data.length;i++){
            list.append(createParamNode(data[i]));
        }
        node.append(list);
        return node;
    }
    /**
     * 参数
     * @param {Object} data 
     * @returns 
     */
    function createParamNode(data){
        let code=$('<code class="param-name">'+data.name+'</code>');
        if(data.flag=='optional'){
            code=$('<code class="param-name optional">['+data.name+']</code>');
        }
        let node=$('<li class="param"></li>');
        node.append(code);
        node.append(createTypeNode(data.type));
        if(data.flag=='optional'){
            node.append('<span class="flag optional" title="This parameter is optional.">optional</span>');
        }
        //参数描述
        node.append(createParamDescription(data.des));
        return node;
    }
    /**
     * 参数类型
     * @param {String|Array} data 
     * @returns 
     */
    function createTypeNode(data){
        let node=$('<span class="type"></span>');
        if(typeof data=='string'){
            node.append(createCrossLink(data));
        }else if(typeof data=='object'){
            for(let i=0;i<data.length;i++){
                node.append(createCrossLink(data[i]));
                if(i<data.length-1)
                node.append(' | ')
            }
        }
        return node;
    }
    /**
     * 参数描述
     * @param {String} des 
     * @returns 
     */
    function createParamDescription(des){
        let node=$('<div class="param-description"><p>'+des+'</p></div>');
        return node;
    }
    function createReturnsNode(data){
        let node=$('<div class="returns"><h4>Returns:</h4></div>');
        node.append(createReturnsDescription(data));
        return node;
    }
    function createReturnsDescription(data){
        let node=$('<div class="returns-description"></div>');
        node.append(createTypeNode(data.type));
        node.append(':')
        node.append('<p> '+data.des+'</p>');
        return node;
    }
}